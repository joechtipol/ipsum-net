/* 
 * Licensed to the Apache Software Foundation (ASF) under one 
 * or more contributor license agreements.  See the NOTICE file 
 * distributed with this work for additional information 
 * regarding copyright ownership.  The ASF licenses this file 
 * to you under the Apache License, Version 2.0 (the 
 * "License"); you may not use this file except in compliance 
 * with the License.  You may obtain a copy of the License at 
 * 
 * http://www.apache.org/licenses/LICENSE-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, 
 * software distributed under the License is distributed on an 
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY 
 * KIND, either express or implied.  See the License for the 
 * specific language governing permissions and limitations 
 * under the License. 
 */ 
 
/* 
 * The sample smart contract for documentation topic: 
 * Writing Your First Blockchain Application 
 */ 
 
package main 
 
/* Imports 
 * 4 utility libraries for formatting, handling bytes, reading and writing JSON, and string manipulation 
 * 2 specific Hyperledger Fabric specific libraries for Smart Contracts 
 */ 
import ( 
    "bytes" 
    "encoding/json" 
    "fmt" 
     
    "time" 
    "github.com/hyperledger/fabric/core/chaincode/shim" 
    sc "github.com/hyperledger/fabric/protos/peer" 
) 
 
// Define the Smart Contract structure 
type SmartContract struct { 
} 
 
type Student struct {  
  Studentid       string   `json:"studentid"` 
  Firstname      string   `json:"firstname"` 
  Lastname      string   `json:"lastname"` 
  Birthdate       string      `json:"birthdate"`  
  Nationality      string   `json:"nationality"` 
  Email      string   `json:"email"` 
  Postaladress      string   `json:"postaladress"` 
  Nic      string   `json:"nic"` 
  Loginid      string   `json:"loginid"` 
  Password      string   `json:"password"` 
  Creationdate       string      `json:"creationdate"`
  DigitalCredentials    []string `json:"digitalCredentials"` 

} 
type DigitalCredential struct {  
  Digitalcredid       string   `json:"digitalcredid"` 
  Hashipfs      string   `json:"hashipfs"` 
  Attributiondate       string      `json:"attributiondate"`  
  Programname      string   `json:"programname"` 
  Degree      string   `json:"degree"` 
  University      string   `json:"university"` 
  Type_digital_credential      string   `json:"type_digital_credential"` 
  Status      string   `json:"status"` 
  Issuedate       string      `json:"issuedate"`  
  Studentid       string   `json:"studentid"` 
  Firstname      string   `json:"firstname"` 
  Lastname      string   `json:"lastname"` 
  Birthdate       string      `json:"birthdate"`  
  Nationality      string   `json:"nationality"` 
  Email      string   `json:"email"` 
  Postaladress      string   `json:"postaladress"` 
  Nic      string   `json:"nic"` 
  Shareds    []string `json:"shareds"` 
} 
 
 
type SharedDigitalCredential struct {  
  Sharedcredid       string   `json:"sharedcredid"` 
  Secretkey      string   `json:"secretkey"` 
  Active       string      `json:"active"`
  Emailshared      string   `json:"emailshared"` 
  Digitalcredid       string   `json:"digitalcredid"` 
  Studentid       string   `json:"studentid"`  
  Hashipfs      string   `json:"hashipfs"` 
  Attributiondate       string      `json:"attributiondate"`  
  Programname      string   `json:"programname"`
  Degree      string   `json:"degree"`  
  University      string   `json:"university"` 
  Type_digital_credential      string   `json:"type_digital_credential"` 
  Status      string   `json:"status"` 
  Shareddate      string   `json:"shareddate"` 
} 


type CollectionStudentDig struct {  
  Studentid       string   `json:"studentid"` 
  Firstname      string   `json:"firstname"` 
  Lastname      string   `json:"lastname"` 
  Birthdate       string      `json:"birthdate"`  
  Nationality      string   `json:"nationality"` 
  Email      string   `json:"email"` 
  Nic      string   `json:"nic"` 
  Digitalcredid       string   `json:"digitalcredid"` 
  Hashipfs      string   `json:"hashipfs"` 
  Attributiondate       string      `json:"attributiondate"`  
  Programname      string   `json:"programname"` 
  University      string   `json:"university"` 
  Type_digital_credential      string   `json:"type_digital_credential"` 
  Status      string   `json:"status"` 
  Issuedate       string      `json:"issuedate"`
} 

 
/* 
 * The Init method is called when the Smart Contract "fabcar" is instantiated by the blockchain network 
 * Best practice is to have any Ledger initialization in separate function -- see initLedger() 
 */ 
func (s *SmartContract) Init(APIstub shim.ChaincodeStubInterface) sc.Response { 
    return shim.Success(nil) 
} 
 
/* 
 * The Invoke method is called as a result of an application request to run the Smart Contract "fabcar" 
 * The calling application program has also specified the particular smart contract function to be called, with arguments 
 */ 
func (s *SmartContract) Invoke(APIstub shim.ChaincodeStubInterface) sc.Response { 
 
    // Retrieve the requested Smart Contract function and arguments 
    function, args := APIstub.GetFunctionAndParameters() 
    // Route to the appropriate handler function to interact with the ledger appropriately 
 
    if function == "initLedger" { 
        return s.initLedger(APIstub) 
    }else if function == "queryStudentCredential" { 
        return s.queryStudentCredential(APIstub, args) 
    } else if function == "searchStudent" { 
        return s.searchStudentByBirthFirstLastNameAttr(APIstub, args) 
    }else if function == "loginStudent" { 
        return s.loginStudent(APIstub, args) 
    }else if function == "searchDigitalCreds" { 
        return s.queryAllCredentials(APIstub, args) 
    }else if function == "addStudent" { 
        return s.addStudent(APIstub, args) 
    } else if function == "addDigitalCredential" { 
        return s.addDigitalCredential(APIstub, args) 
    }else if function == "removeDigitalCredential" { 
        return s.removeDigitalCredential(APIstub, args) 
    }else if function == "revokeDigitalCredential" { 
        return s.revokeDigitalCredential(APIstub, args) 
    }else if function == "acceptDigitalCredential" { 
        return s.acceptDigitalCredential(APIstub, args) 
    } else if function == "shareDigitalCredential" { 
        return s.shareDigitalCredential(APIstub, args) 
    }else if function == "revokeShareDigitalCredential" { 
        return s.revokeShareDigitalCredential(APIstub, args) 
    }else if function == "searchSharedDigitalCreds" { 
        return s.searchSharedDigitalCreds(APIstub, args) 
    }else if function == "queryAllStudentsCredential" { 
        return s.queryAllStudentsCredential(APIstub) 
    }else if function == "queryAllCredential" { 
        return s.queryAllCredential(APIstub) 
    } 
 
    return shim.Error("Invalid Smart Contract function name.") 
} 
 
func (s *SmartContract) queryStudentCredential(APIstub shim.ChaincodeStubInterface, args []string) sc.Response { 
 
    if len(args) != 1 { 
        return shim.Error("Incorrect number of arguments. Expecting 1") 
    } 
 
    sudentAsBytes, _ := APIstub.GetState("STD_"+args[0]) 
    return shim.Success(sudentAsBytes) 
} 


  func (s *SmartContract)  searchStudentByBirthFirstLastNameAttr(stub shim.ChaincodeStubInterface,args []string) sc.Response { 
 
 
    queryString := "{\"selector\":{\"_id\": {\"$gt\": \"STD_\"},\"firstname\": {\"$regex\": \""+args[0]+"\"},\"lastname\": {\"$regex\": \""+args[1]+"\"},\"birthdate\": {\"$regex\": \""+args[2]+"\"}}}" 

    queryResults, err := getQueryResultForQueryString(stub, queryString) 
    if err != nil { 
        return shim.Error("Problem with search query") 
    } 
     return shim.Success(queryResults)       
} 
  
  func (s *SmartContract)  loginStudent(stub shim.ChaincodeStubInterface,args []string) sc.Response { 
 
 
    queryString := "{\"selector\":{\"_id\": {\"$gt\": \"STD_\"},\"loginid\": {\"$eq\": \""+args[0]+"\"},\"password\": {\"$eq\": \""+args[1]+"\"}}}" 

    queryResults, err := getQueryResultForQueryString(stub, queryString) 
    if err != nil { 
        return shim.Error("Problem with search query") 
    } 
     return shim.Success(queryResults)       
} 
 
func (s *SmartContract) initLedger(APIstub shim.ChaincodeStubInterface) sc.Response { 
        var dt = time.Now() 
         
        student:=Student{Studentid:"00000001", 
        Firstname:"test-student-first-name", 
        Lastname:"test-student-last-name", 
        Birthdate:"1988-07-25", 
        Nationality:"FRANCE", 
        Email:"ipscr@yopmail.com", 
        Postaladress:"quartie general des blockchaineurs 12900 Le monde", 
        Nic:"A1B2C3D4",
        Loginid:"testloginid", 
        Password:"697073756d", 
        Creationdate:dt.String(), 
        DigitalCredentials:[]string{},
        } 
         
        studentAsBytes, _ := json.Marshal(student) 
        APIstub.PutState("STD_"+student.Studentid, studentAsBytes) 
        fmt.Println("Added", student) 
 
 
    return shim.Success(nil) 
} 
 
func (s *SmartContract) addStudent(APIstub shim.ChaincodeStubInterface, args []string) sc.Response { 
    var dt = time.Now() 
 
    if len(args) < 8 { 
        return shim.Error("Incorrect number of arguments. Expecting 8") 
    } 
     
     testStudent,_:=queryStudentByEmail(APIstub,args[5])
     
     if(testStudent.Studentid!=""){
     	return shim.Error("Please put another email because email is already registered in the network") 
     }
 
     
    var student = Student{Studentid: args[0],Firstname: args[1],
    Lastname: args[2], Birthdate: args[3], Nationality: args[4],
    Email: args[5],Postaladress: args[6],Nic: args[7],Creationdate: dt.String(),
    DigitalCredentials:[]string{},
    Loginid:args[8],Password: "697073756d"} 
 
    studentAsBytes, _ := json.Marshal(student) 
    APIstub.PutState("STD_"+args[0], studentAsBytes) 
 
    return shim.Success(studentAsBytes) 
} 
 
func (s *SmartContract) addDigitalCredential(APIstub shim.ChaincodeStubInterface, args []string) sc.Response { 
    var dt = time.Now() 
 
    if len(args) < 5 { 
        return shim.Error("Incorrect number of arguments. Expecting 5") 
    } 
        studentAsBytes, _ := APIstub.GetState("STD_"+args[1])
        
        studentToTransfer := Student{}
	     json.Unmarshal(studentAsBytes, &studentToTransfer) //unmarshal it aka JSON.parse()
        	
        
        currentDcs:=studentToTransfer.DigitalCredentials
        
             
        var digitalCredential = DigitalCredential{ 
             Digitalcredid: args[0], 
             Attributiondate: args[2],  
             Programname: args[3],
             Degree: args[4],  
             University: args[5], 
             Type_digital_credential: args[6], 
             Hashipfs: args[7],  
             Status: "PENDING",
             Issuedate: dt.String(), 
             Studentid: studentToTransfer.Studentid,
			 Firstname: studentToTransfer.Firstname, 
			 Lastname: studentToTransfer.Lastname, 
			 Birthdate: studentToTransfer.Birthdate,  
			 Nationality: studentToTransfer.Nationality, 
			 Email: studentToTransfer.Email,
			 Postaladress: studentToTransfer.Postaladress,
			 Nic: studentToTransfer.Nic,
             Shareds: []string{},
        } 
        
        currentDcs=append(currentDcs,args[0])
        
        studentToTransfer.DigitalCredentials=currentDcs
        
        digitalCredAsBytes, _ := json.Marshal(digitalCredential) 
        APIstub.PutState("DIG_"+args[0], digitalCredAsBytes) 

 
        studentAsBytes, _ = json.Marshal(studentToTransfer) 
        APIstub.PutState("STD_"+args[1], studentAsBytes) 
     
        return shim.Success(digitalCredAsBytes) 
} 
 
 
func (s *SmartContract) removeDigitalCredential(APIstub shim.ChaincodeStubInterface, args []string) sc.Response { 
 
    if len(args) < 1 { 
        return shim.Error("Incorrect number of arguments. Expecting 1 (digitalcredid)") 
    } 
    
    
       var digitalcredid= args[0]
     
        digitalcredAsBytes, _ := APIstub.GetState("DIG_"+digitalcredid)
        
	    APIstub.DelState("DIG_"+digitalcredid) 
	 
	    return shim.Success(digitalcredAsBytes) 
}  
 
func (s *SmartContract) revokeDigitalCredential(APIstub shim.ChaincodeStubInterface, args []string) sc.Response { 
    var dt = time.Now() 
 
    if len(args) < 1 { 
        return shim.Error("Incorrect number of arguments. Expecting 1 (digitalcredid)") 
    } 
    
    
       var digitalcredid= args[0]
     
        digitalcredAsBytes, _ := APIstub.GetState("DIG_"+digitalcredid)
        
        digitalcredToTransfer := DigitalCredential{}
	    json.Unmarshal(digitalcredAsBytes, &digitalcredToTransfer) //unmarshal it aka JSON.parse()
        
        digitalcredToTransfer.Status="INVALID"
        digitalcredToTransfer.Issuedate= dt.String()
     
 
	    digitalCredentialAsBytes, _ := json.Marshal(digitalcredToTransfer) 
	    APIstub.PutState("DIG_"+args[0], digitalCredentialAsBytes) 
	 
	    return shim.Success(digitalCredentialAsBytes) 
} 
 
 
 func (s *SmartContract) acceptDigitalCredential(APIstub shim.ChaincodeStubInterface, args []string) sc.Response { 
    var dt = time.Now() 
 
    if len(args) < 1 { 
        return shim.Error("Incorrect number of arguments. Expecting 1 (digitalcredid)") 
    } 
    
    
       var digitalcredid= args[0]
     
        digitalcredAsBytes, _ := APIstub.GetState("DIG_"+digitalcredid)
        
        digitalcredToTransfer := DigitalCredential{}
	    json.Unmarshal(digitalcredAsBytes, &digitalcredToTransfer) //unmarshal it aka JSON.parse()
        
        digitalcredToTransfer.Status="VALID"
        digitalcredToTransfer.Issuedate= dt.String()
     
 
	    digitalCredentialAsBytes, _ := json.Marshal(digitalcredToTransfer) 
	    APIstub.PutState("DIG_"+args[0], digitalCredentialAsBytes) 
	 
	    return shim.Success(digitalCredentialAsBytes) 
} 
 // Find returns the smallest index i at which x == a[i],
// or len(a) if there is no such index.
func Find(a []string, x string) int {
    for i, n := range a {
        if (x == n) {
            return i
        }
    }
    return len(a)
}
 
func (s *SmartContract) shareDigitalCredential(APIstub shim.ChaincodeStubInterface, args []string) sc.Response { 
    var dt = time.Now() 
 
    if len(args) < 2 { 
        return shim.Error("Incorrect number of arguments. Expecting 2") 
    } 
    
         
        digitalcredAsBytes, _ := APIstub.GetState("DIG_"+args[1])
        
        digitalcredToTransfer := DigitalCredential{}
	    json.Unmarshal(digitalcredAsBytes, &digitalcredToTransfer) //unmarshal it aka JSON.parse()
      

        var sharedDigitalCredential = SharedDigitalCredential{Sharedcredid: args[0],
             Secretkey: args[2],
             Active: "1",
             Emailshared: args[3],
             Digitalcredid: digitalcredToTransfer.Digitalcredid,
             Studentid: digitalcredToTransfer.Studentid,
			 Hashipfs: digitalcredToTransfer.Hashipfs,
			 Attributiondate: digitalcredToTransfer.Attributiondate,
			 Programname: digitalcredToTransfer.Programname,
			 Degree: digitalcredToTransfer.Degree,
			 University: digitalcredToTransfer.University,
			 Type_digital_credential: digitalcredToTransfer.Type_digital_credential,
			 Status: digitalcredToTransfer.Status,
             Shareddate: dt.String(),
        } 
        
        currentShareds:=digitalcredToTransfer.Shareds

        currentShareds=append(currentShareds,args[0])
        
        digitalcredToTransfer.Shareds=currentShareds
        
        
        sharedDigitalCredentialAsBytes, _ := json.Marshal(sharedDigitalCredential) 
        
        errorShare := APIstub.PutState("SHARE_"+args[0], sharedDigitalCredentialAsBytes) 
        if(errorShare!=nil){
        	return shim.Error(errorShare.Error())
        }
        
        digitalCredToTransferAsBytes, _ := json.Marshal(digitalcredToTransfer) 
        
     
        errorDigCred :=  APIstub.PutState("DIG_"+args[1], digitalCredToTransferAsBytes)
        if(errorDigCred!=nil){
        	return shim.Error(errorDigCred.Error())
        }
     
        return shim.Success(sharedDigitalCredentialAsBytes) 
} 
 
 
func (s *SmartContract) revokeShareDigitalCredential(APIstub shim.ChaincodeStubInterface, args []string) sc.Response { 
 
    if len(args) < 1 { 
        return shim.Error("Incorrect number of arguments. Expecting 1") 
    } 
     
       var sharedcredid= args[0]
     
        sharedDigitalCredentialAsBytes, _ := APIstub.GetState("SHARE_"+sharedcredid)
        
        sharedCredToTransfer := SharedDigitalCredential{}
	    json.Unmarshal(sharedDigitalCredentialAsBytes, &sharedCredToTransfer) //unmarshal it aka JSON.parse()
        
        sharedCredToTransfer.Active="0"
     
 
	    sharedDigitalCredentialAsBytes, _ = json.Marshal(sharedCredToTransfer) 
	    APIstub.PutState("SHARE_"+sharedcredid, sharedDigitalCredentialAsBytes) 
	 
	    return shim.Success(sharedDigitalCredentialAsBytes) 
} 
 
 
  func queryStudentByEmail(stub shim.ChaincodeStubInterface,email string) (Student, error) { 
 
 
    queryString := "{\"selector\":{\"_id\": {\"$regex\": \"STD_\"},\"email\": {\"$eq\": \""+email+"\"}}}" 
     studentToTransfer := Student{}

    queryResults, err := getSingleQueryResultForQueryString(stub, queryString) 
    if err != nil { 
        return studentToTransfer,err
    } 
    
    json.Unmarshal(queryResults, &studentToTransfer) //unmarshal it aka JSON.parse()
    
    
    return studentToTransfer,nil
}
 
 func queryStudentByDigiCredId(stub shim.ChaincodeStubInterface,digiCredId string) (Student, error) { 
 
 
    queryString := "{\"selector\":{\"_id\": {\"$gt\": \"STD_\"},\"digitalCredentials\": {\"$elemMatch\": {\"digitalcredid\": \""+digiCredId+"\"}}}}" 
     studentToTransfer := Student{}

    queryResults, err := getSingleQueryResultForQueryString(stub, queryString) 
    if err != nil { 
        return studentToTransfer,err
    } 
    
    json.Unmarshal(queryResults, &studentToTransfer) //unmarshal it aka JSON.parse()
    
    
    return studentToTransfer,nil
} 


func (s *SmartContract) queryAllCredentials(stub shim.ChaincodeStubInterface, args []string) sc.Response { 
 
   var firstname = args[0] 

  var lastname = args[1]

   var programname = args[2]
   
   var degree = args[3]

    var university = args[4]
     var type_digital_credential= args[5]
   var studentId = args[6]
    var digitCredId = args[7]
    
    
    queryString := "{\"selector\":{\"_id\": {\"$regex\": \"DIG_\"},\"programname\": {\"$regex\": \""+programname+"\"},\"degree\": {\"$regex\": \""+degree+"\"},\"university\": {\"$regex\": \""+university+"\"},\"digitalcredid\": {\"$regex\": \""+digitCredId+"\"},\"firstname\": {\"$regex\": \""+firstname+"\"},\"lastname\": {\"$regex\": \""+lastname+"\"},\"studentid\": {\"$regex\": \""+studentId+"\"},\"type_digital_credential\": {\"$regex\": \""+type_digital_credential+"\"}}}" 
 
    queryResults, err := getQueryResultForQueryString(stub, queryString) 
    if err != nil { 
        return shim.Error(err.Error()) 
    } 
    return shim.Success(queryResults) 
} 

func (s *SmartContract) searchSharedDigitalCreds(stub shim.ChaincodeStubInterface, args []string) sc.Response { 
 
   var studentId = args[0] 

   var digitCredId = args[1]

   var shareDigitCredId = args[2]
   
   var active = args[3]
   
   
   var secretKey = args[4]


    //queryString := "{\"selector\":{\"_id\": {\"$regex\": \"DIG_\"},\"digitalcredid\": {\"$regex\": \""+digitCredId+"\"},\"studentid\": {\"$regex\": \""+studentId+"\"},\"shareds\": {\"$elemMatch\": {\"$and\": [{\"active\": {\"$eq\": \""+active+"\"}},{\"sharedcredid\": {\"$regex\": \""+shareDigitCredId+"\"}},{\"secretkey\": {\"$regex\": \""+secretKey+"\"}}] }}}}" 


    queryString := "{\"selector\":{\"_id\": {\"$regex\": \"SHARE_\"},\"digitalcredid\": {\"$regex\": \""+digitCredId+"\"},\"studentid\": {\"$regex\": \""+studentId+"\"},\"active\": {\"$eq\": \""+active+"\"},\"sharedcredid\": {\"$regex\": \""+shareDigitCredId+"\"},\"secretkey\": {\"$regex\": \""+secretKey+"\"}}}" 


 
    queryResults, err := getQueryResultForQueryString(stub, queryString) 
    if err != nil { 
        return shim.Error(err.Error()) 
    } 
    return shim.Success(queryResults) 
} 
 
func (s *SmartContract) queryAllStudentsCredential(stub shim.ChaincodeStubInterface) sc.Response { 
 
    queryString := "{\"selector\":{\"_id\": {\"$regex\": \"STD_\"}}}" 
 
    queryResults, err := getQueryResultForQueryString(stub, queryString) 
    if err != nil { 
        return shim.Error(err.Error()) 
    } 
    return shim.Success(queryResults) 
} 
 
 
  // ========================================================================================= 
// getQueryResultForQueryString executes the passed in query string. 
// Result set is built and returned as a byte array containing the JSON results. 
// ========================================================================================= 
func getQueryResultForDigCreds(stub shim.ChaincodeStubInterface, queryString string) ([]byte, error) { 
 
    fmt.Printf("- getQueryResultForDigCreds queryString:\n%s\n", queryString) 
 
    resultsIterator, err := stub.GetQueryResult(queryString) 
    if err != nil { 
        return nil, err 
    } 
    defer resultsIterator.Close() 
 
    buffer, err := constructDigQueryResponseFromIterator(stub,resultsIterator) 
    if err != nil { 
        return nil, err 
    } 
 
    fmt.Printf("- getQueryResultForDigCreds queryResult:\n%s\n", buffer.String()) 
 
    return buffer.Bytes(), nil 
} 
 
 
 // ========================================================================================= 
// getQueryResultForQueryString executes the passed in query string. 
// Result set is built and returned as a byte array containing the JSON results. 
// ========================================================================================= 
func getSingleQueryResultForQueryString(stub shim.ChaincodeStubInterface, queryString string) ([]byte, error) { 
 
    fmt.Printf("- getQueryResultForQueryString queryString:\n%s\n", queryString) 
 
    resultsIterator, err := stub.GetQueryResult(queryString) 
    if err != nil { 
        return nil, err 
    } 
    defer resultsIterator.Close() 
 
    buffer, err := constructSingleQueryResponseFromIterator(resultsIterator) 
    if err != nil { 
        return nil, err 
    } 
 
    fmt.Printf("- getQueryResultForQueryString queryResult:\n%s\n", buffer.String()) 
 
    return buffer.Bytes(), nil 
} 
 
// ========================================================================================= 
// getQueryResultForQueryString executes the passed in query string. 
// Result set is built and returned as a byte array containing the JSON results. 
// ========================================================================================= 
func getQueryResultForQueryString(stub shim.ChaincodeStubInterface, queryString string) ([]byte, error) { 
 
    fmt.Printf("- getQueryResultForQueryString queryString:\n%s\n", queryString) 
 
    resultsIterator, err := stub.GetQueryResult(queryString) 
    if err != nil { 
        return nil, err 
    } 
    defer resultsIterator.Close() 
 
    buffer, err := constructArrayQueryResponseFromIterator(resultsIterator) 
    if err != nil { 
        return nil, err 
    } 
 
    fmt.Printf("- getQueryResultForQueryString queryResult:\n%s\n", buffer.String()) 
 
    return buffer.Bytes(), nil 
} 
 
 
 
// =========================================================================================== 
// constructQueryResponseFromIterator constructs a JSON array containing query results from 
// a given result iterator 
// =========================================================================================== 
func constructQueryResponseFromIterator(resultsIterator shim.StateQueryIteratorInterface) (*bytes.Buffer, error) { 
    // buffer is a JSON array containing QueryResults 
    var buffer bytes.Buffer 
    buffer.WriteString("[") 
 
    bArrayMemberAlreadyWritten := false 
    for resultsIterator.HasNext() { 
        queryResponse, err := resultsIterator.Next() 
        if err != nil { 
            return nil, err 
        } 
        // Add a comma before array members, suppress it for the first array member 
        if bArrayMemberAlreadyWritten == true { 
            buffer.WriteString(",") 
        } 
        buffer.WriteString("{\"Key\":") 
        buffer.WriteString("\"") 
        buffer.WriteString(queryResponse.Key) 
        buffer.WriteString("\"") 
 
        buffer.WriteString(", \"Record\":") 
        // Record is a JSON object, so we write as-is 
        buffer.WriteString(string(queryResponse.Value)) 
        buffer.WriteString("}") 
        bArrayMemberAlreadyWritten = true 
    } 
    buffer.WriteString("]") 
 
    return &buffer, nil 
} 


// =========================================================================================== 
// constructQueryResponseFromIterator constructs a JSON array containing query results from 
// a given result iterator 
// =========================================================================================== 
func constructSingleQueryResponseFromIterator(resultsIterator shim.StateQueryIteratorInterface) (*bytes.Buffer, error) { 
    // buffer is a JSON array containing QueryResults 
    var buffer bytes.Buffer  
    for resultsIterator.HasNext() { 
        queryResponse, err := resultsIterator.Next() 
        if err != nil { 
            return nil, err 
        } 
        buffer.WriteString(string(queryResponse.Value)) 
        return &buffer, nil
         
    } 
 
    return &buffer, nil 
} 

// =========================================================================================== 
// constructQueryResponseFromIterator constructs a JSON array containing query results from 
// a given result iterator 
// =========================================================================================== 
func constructArrayQueryResponseFromIterator(resultsIterator shim.StateQueryIteratorInterface) (*bytes.Buffer, error) { 
    // buffer is a JSON array containing QueryResults 
    var buffer bytes.Buffer  
    buffer.WriteString("[") 
    bArrayMemberAlreadyWritten := false 
    
    for resultsIterator.HasNext() { 
        queryResponse, err := resultsIterator.Next() 
        if err != nil { 
            return nil, err 
        } 
        // Add a comma before array members, suppress it for the first array member 
        if bArrayMemberAlreadyWritten == true { 
            buffer.WriteString(",") 
        } 
        buffer.WriteString(string(queryResponse.Value))          
        bArrayMemberAlreadyWritten = true 
    } 
    buffer.WriteString("]") 
 
    return &buffer, nil 
} 

// =========================================================================================== 
// constructQueryResponseFromIterator constructs a JSON array containing query results from 
// a given result iterator 
// =========================================================================================== 
func constructDigQueryResponseFromIterator(stub shim.ChaincodeStubInterface,resultsIterator shim.StateQueryIteratorInterface) (*bytes.Buffer, error) { 
    // buffer is a JSON array containing QueryResults 
    var buffer bytes.Buffer  
    buffer.WriteString("[") 
    bArrayMemberAlreadyWritten := false 
    
    for resultsIterator.HasNext() { 
        queryResponse, err := resultsIterator.Next() 
        if err != nil { 
            return nil, err 
        } 
        // Add a comma before array members, suppress it for the first array member 
        if bArrayMemberAlreadyWritten == true { 
            buffer.WriteString(",") 
        } 
        
        currentDigCred := CollectionStudentDig{}
	    json.Unmarshal(queryResponse.Value, &currentDigCred) //unmarshal it aka JSON.parse()
        
        var digitalcredid=currentDigCred.Digitalcredid
        testStudent,_:=queryStudentByDigiCredId(stub,digitalcredid)

        currentDigCred.Firstname= testStudent.Firstname
        currentDigCred.Lastname= testStudent.Lastname
        currentDigCred.Birthdate= testStudent.Birthdate
        currentDigCred.Email= testStudent.Email
        currentDigCred.Nic= testStudent.Nic
        currentDigCred.Studentid= testStudent.Studentid
        
        currentDigCredAsBytes, _ := json.Marshal(currentDigCred) 
        
        
        buffer.WriteString(string(currentDigCredAsBytes))          
        bArrayMemberAlreadyWritten = true 
    } 
    buffer.WriteString("]") 
 
    return &buffer, nil 
} 





 
 
func (s *SmartContract) queryAllCredential(APIstub shim.ChaincodeStubInterface) sc.Response { 
 
    startKey := "00000000" 
    endKey := "99999999" 
 
    resultsIterator, err := APIstub.GetStateByRange(startKey, endKey) 
    if err != nil { 
        return shim.Error(err.Error()) 
    } 
    defer resultsIterator.Close() 
 
    // buffer is a JSON array containing QueryResults 
    var buffer bytes.Buffer 
    buffer.WriteString("[") 
 
    bArrayMemberAlreadyWritten := false 
    for resultsIterator.HasNext() { 
        queryResponse, err := resultsIterator.Next() 
        if err != nil { 
            return shim.Error(err.Error()) 
        } 
        // Add a comma before array members, suppress it for the first array member 
        if bArrayMemberAlreadyWritten == true { 
            buffer.WriteString(",") 
        } 
        buffer.WriteString("{\"Key\":") 
        buffer.WriteString("\"") 
        buffer.WriteString(queryResponse.Key) 
        buffer.WriteString("\"") 
 
        buffer.WriteString(", \"Record\":") 
        // Record is a JSON object, so we write as-is 
        buffer.WriteString(string(queryResponse.Value)) 
        buffer.WriteString("}") 
        bArrayMemberAlreadyWritten = true 
    } 
    buffer.WriteString("]") 
 
    fmt.Printf("- queryAllStudents:\n%s\n", buffer.String()) 
 
    return shim.Success(buffer.Bytes()) 
} 
 
 
 
 
 
// The main function is only relevant in unit test mode. Only included here for completeness. 
func main() { 
 
    // Create a new Smart Contract 
    err := shim.Start(new(SmartContract)) 
    if err != nil { 
        fmt.Printf("Error creating new Smart Contract: %s", err) 
    } 
} 